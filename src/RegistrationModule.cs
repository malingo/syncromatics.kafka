﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;

namespace Syncromatics.Kafka
{
    public class RegistrationModule : Module
    {
        private readonly KafkaSettings _settings;

        public RegistrationModule(KafkaSettings settings)
        {
            _settings = settings;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(_settings);
        }
    }
}
