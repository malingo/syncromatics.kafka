﻿using System;
using System.Collections.Generic;
using System.Text;
using Syncromatics.Kafka.PostProcess.Messages;

namespace Syncromatics.Kafka.PostProcess
{
    public interface IPostProcessConsumer
    {
        ConsumeResult<NonRealTimeDataProcessedMessage> Consume(TimeSpan timeout);
    }
}
