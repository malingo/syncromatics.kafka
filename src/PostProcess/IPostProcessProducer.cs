﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Syncromatics.Kafka.PostProcess.Messages;

namespace Syncromatics.Kafka.PostProcess
{
    public interface IPostProcessProducer
    {
        Task<DeliveryResult<NonRealTimeDataProcessedMessage>> ProduceAsync(
            NonRealTimeDataProcessedMessage message);

        void BeginProduce(
            NonRealTimeDataProcessedMessage message,
            Action<DeliveryResult<NonRealTimeDataProcessedMessage>> callback);
    }
}
