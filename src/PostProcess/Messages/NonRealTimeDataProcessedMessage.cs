﻿using System;

namespace Syncromatics.Kafka.PostProcess.Messages
{
    public class NonRealTimeDataProcessedMessage
    {
        public long VehicleId { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }
    }
}
